package ru.t1.simanov.tm.api;

import ru.t1.simanov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
